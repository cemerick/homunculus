type json = Yojson.Basic.t

val equal_json : json -> json -> bool

type jsonObject = [ `Assoc of (string * json) list ]

val pp_json : Format.formatter -> Yojson.Basic.t -> unit

module Environment : sig
  type id = EID of int

  val newid : unit -> id
end

type output =
  | Stdout
  | Stderr

type millis = Ms of int

type envOption =
  [ `NoCapture of output
  | `EnvTTL of millis
  ]

module Message : sig
  type t = Message of jsonObject
  type id = MID of int

  val to_json : t -> jsonObject
  val to_string : jsonObject -> string

  val create :
       ?eid:Environment.id
    -> ?code:string
    -> ?return:bool
    -> ?options:envOption list
    -> id
    -> t
end

module Response : sig
  type prints =
    [ `Stderr of string
    | `Stdout of string
    ]

  type values =
    [ `Return of json
    | `Value of string option * json
    ]

  type error = [ `Error of string ]

  type t =
    [ prints
    | values
    | error
    ]

  val pp : Format.formatter -> t -> unit

  type routable =
    { env : Environment.id
    ; mid : Message.id option
    ; data : t
    }

  val pp_routable : Format.formatter -> routable -> unit
  (* val routable_of_json : string -> routable *)
end

module Process : sig
  type t

  type option =
    [ envOption
    | `ModulePath of string
    ]

  val start : ?options:option list -> unit -> t
  val stop : t -> unit

  val send :
       ?close:bool
    -> Environment.id
    -> t
    -> Message.id
    -> jsonObject
    -> Response.t Lwt_stream.t
end

val eval :
     ?process:Process.t
  -> ?eid:Environment.id
  -> ?options:envOption list
  -> ?return:bool
  -> string
  -> Response.t Lwt_stream.t

val return_result : Response.t Lwt_stream.t -> (json, string) result Lwt.t

type aggregate =
  { all : Response.t list
  ; prints : Response.prints list
  ; values : (string option * json) list
  ; result : (json, string) result
  }

val aggregated_result : Response.t Lwt_stream.t -> aggregate Lwt.t