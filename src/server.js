let createDomain = require("domain").create;
let vm = require("vm");
let fs = require("fs");
let readline = require('readline');
let events = require('events');
let { Readable } = require("stream")

let origStdout = process.stdout.write.bind(process.stdout);

let write = (target, original) => (chunk, encoding, callback) => {
  let domain = process.domain;

  if (!domain || domain.environment.config.passthrough) original(chunk, encoding, callback);

  if (domain && domain.environment && domain.environment.config[target]) {
    // TODO deal with ANSI output, see "strip-ansi" library
    domain.environment.console(target, chunk);
  }
}

process.stdout.write = write("stdout", process.stdout.write.bind(process.stdout));
process.stderr.write = write("stderr", process.stderr.write.bind(process.stderr));

let globalInit = { require };
(function () {
  // explicitly not including sys because simply dereferecing it out the globals object causes it to be loaded!?
  // https://github.com/nodejs/node/issues/40871
  Object.getOwnPropertyNames(this).forEach(k => { if (k != "sys") globalInit[k] = this[k] });
})();

class Environment {

  constructor(id, config) {
    this.config = Object.assign({}, config);
    this.id = id;
    this.context = vm.createContext(Object.assign({}, globalInit));
    this.renew(config.ttl);
  }

  renew(ttl) {
    this.expires = Math.max(this.expires || 0, Date.now() + (ttl || this.config.ttl));
  }

  copy(id) {
    let copy = Object.assign(Object.create(Object.getPrototypeOf(this)), this);
    copy.id = id;
    return copy;
  }
  run(code, mid, ret) {
    // an environment object exclusively for this message
    let mthis = mid ? this.copy([this.id, mid]) : this;

    let domain = createDomain();
    domain.environment = mthis;
    domain.on("error", e => mthis.error(e));
    // this dumb timeout is here to work around https://github.com/nodejs/node/issues/40999
    // TODO this issue has been resolved as of 07/2022
    setTimeout(_ => {
      domain.run(() => {
        var s;
        try {
          s = new vm.Script(code);
        } catch (e) {
          mthis.error(e);
        }
        if (s) {
          s.runInContext(this.context)(mthis)
            .then(v => { if (ret) mthis.return(v) })
            .catch(e => mthis.error(e));
        }
      })
    }, 0);
  }

  send(msg) {
    msg.unshift(this.id);
    this.config.output(JSON.stringify(msg));
    this.config.output("\n");
  }

  return(v) {
    this.renew();
    this.send(["return", v]);
  }
  emit(v, annotation) {
    this.renew();
    this.send(typeof annotation != "undefined" ? ["value", v, annotation] : ["value", v]);
  }
  error(e) {
    this.renew();
    this.send(["error", e.toString()]);
  }
  console(source, content) {
    this.renew();
    this.send([source, content]);
  }
}

class Configuration {
  static default = {
    stderr: true,
    stdout: true,
    output: origStdout,
    passthrough: false,
    // 60s
    ttl: (1000 * 60)
  };

  static exit(msg) {
    console.log(msg);
    process.exit(1);
  }

  static usage() {
    console.log("Usage: TODO");
    process.exit(0);
  }

  static read(args) {
    let conf = Object.assign({}, Configuration.default);
    function bool_of(argv) {
      let [arg, s] = argv;
      try {
        let v = JSON.parse(s);
        if (!(typeof v == "boolean")) throw SyntaxError();
        return v;
      } catch (_) {
        exit(`Unacceptable value for argument ${arg}`);
      }
    }

    if (args.includes("--on")) {
      conf.mode = "server";
    } else {
      conf.mode = "stdio";
    }

    let opts = (args) => {
      if (!args || args.length == 0) return;
      let arg = args[0].split("=");
      switch (arg[0]) {
        case "--help":
          Configuration.usage();
        case "-q":
          conf.quiet = true;
          return opts(args.slice(1));
        case "--ttl":
          if (arg[1]) {
            conf.ttl = parseInt(arg[1]);
            return opts(args.slice(1));
          } else if (args[1]) {
            conf.ttl = parseInt(args[1]);
            return opts(args.slice(2));
          } else {
            exit(`Bad/missing --ttl value`);
          }
      }

      if (conf.mode == "server") {
        switch (args[0]) {
          case "--on":
            // if (typeof args[1] != "string" || !args[1].match(/\d+$/)) exit(`${args[1]} is not a port number`);
            if (!args[1]) exit("No socket path or server address provided to --on");
            conf.serve = args[1];
            return opts(args.slice(2));
          default:
            exit(`Unexpected argument ${args[0]}`);
        }
      } else {
        switch (arg[0]) {
          case "-e":
            if (typeof args[1] != "string") exit(`Missing code to evaluate, bad -e option`);
            conf.expr = args[1];
            return opts(args.slice(2));
          default:
            Configuration.exit(`Unexpected argument ${args[0]}`);
        }
      }
    }

    opts(args);
    return conf;
  }
}

let conf = Configuration.read(process.argv.slice(__filename == "[eval]" ? 1 : 2));
let evalThunk = (code) => `async (homunculus) => {${code}}`

let environments = {};

var waitingReaper = 0;
function reaper() {
  for (let [id, env] of Object.entries(environments)) {
    if (env.expires <= Date.now()) {
      // if (!conf.quiet) console.log(`Reaping expired environment ${id}`);
      delete environments[id];
    }
  }
  waitingReaper = setTimeout(reaper, 1000);
}

let exitHandlers = [() => clearTimeout(waitingReaper)]
function shutdown() {
  for (let f of exitHandlers) {
    f();
  }
}

let run = (input, output) => {
  (async () => {
    const rl = readline.createInterface({
      input,
      crlfDelay: Infinity
    });

    if (conf.mode == "stdio") rl.on("close", shutdown);

    rl.on('line', (line) => {
      if (line.trim().length > 0) {
        var input;
        try {
          input = JSON.parse(line);
        } catch (_) {
          output(JSON.stringify(["", "error", "Homunculus: could not parse message, invalid JSON."]))
          return;
        }

        input = typeof input == "string" ? { env: 0, code: input, ret: true } : input;
        if (input.kill) {
          delete environments[input.kill];
        } else {
          let { env, mid, code, ret, stdout, stderr, passthrough, ttl } = input;
          let envid = env === undefined ? 0 : env;
          let aenv = environments[envid] ? environments[envid] : new Environment(envid, conf);
          environments[envid] = aenv;
          aenv.renew(ttl);
          aenv.config.output = output;
          if (stdout !== undefined) aenv.config.stdout = stdout;
          if (stderr !== undefined) aenv.config.stderr = stderr;
          if (code !== undefined) {
            aenv.run(evalThunk(code), mid, (ret === undefined) || ret);
          }
        }
      }
    });

    await events.once(rl, 'close');
  })();
}

if (conf.serve) {
  let net = require('net');
  let server = net.createServer((c) => {
    c.on('end', () => {
      // maybe GC associated environment
    });
    run(c, c.write.bind(c));
  });
  server.on('error', (err) => {
    throw err;
  });

  if (conf.serve.indexOf(":") > -1) {
    let [host, port] = conf.serve.split(":");
    server.listen(port, host, () => {
      if (!conf.quiet) console.error(`Server ready on :${server.address().port}`)
    });
  } else {
    // assume domain socket path
    server.listen(conf.serve, () => {
      if (!conf.quiet) console.error(`Server ready on \`${conf.serve}\``);
    });
  }

  exitHandlers.push(() => server.close());
  reaper();
} else {
  if (conf.expr != undefined) {
    run(Readable.from([JSON.stringify({ code: conf.expr, env: 1 })]), origStdout);
  } else {
    run(process.stdin, origStdout)
    reaper();
  }
}

process.on('exit', shutdown);
process.on('SIGINT', shutdown);

// TODO option to strongly associate an environment with a single connection; upon disconnection, reap