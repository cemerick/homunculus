open Fun

type json = Yojson.Basic.t [@@deriving eq]
type jsonObject = [ `Assoc of (string * json) list ]

(* A fixed pp for json values that wraps them in parens, suitable for copy-pasting back into code
   as convenient. *)
let pp_json fmt json = Format.fprintf fmt "(%a)" Yojson.Basic.pp json
let ( %> ) f g x = g (f x)

let counter start =
  let next = ref start in
  fun () ->
    next := !next + 1;
    !next

module Environment = struct
  type id = EID of int [@@deriving ord, show { with_path = false }]

  module Map = Map.Make (struct
    type t = id

    let compare = compare_id
  end)

  let nextid = counter 0
  let newid () = EID (nextid ())
end

type output =
  | Stdout
  | Stderr

type millis = Ms of int

type envOption =
  [ `NoCapture of output
  | `EnvTTL of millis
  ]

module Option = struct
  include Option

  let get_lazy default_fn x = match x with None -> default_fn () | Some y -> y
  let get_or default x = match x with None -> default | Some y -> y
  let map_or default f = function None -> default | Some x -> f x
end

module Message = struct
  type t = Message of jsonObject
  type id = MID of int [@@deriving ord, show { with_path = false }]

  module Map = Map.Make (struct
    type t = id

    let compare = compare_id
  end)

  let to_json (Message m) = m

  (* no easier way to set infinite margin / get printed json on one line? *)
  let to_string (json : jsonObject) =
    let open Format in
    let b = Buffer.create 256 in
    let f = Format.formatter_of_buffer b in
    pp_set_margin f Int.max_int;
    Format.fprintf f "%a" (Yojson.Basic.pretty_print ~std:true) (json :> json);
    pp_print_flush f ();
    Buffer.contents b

  let create ?eid ?code ?return ?options (MID mid) : t =
    let open Option in
    let envOpts =
      map_or []
        (List.map (function
          | `NoCapture Stdout   -> "stdout", `Bool false
          | `NoCapture Stderr   -> "stderr", `Bool false
          | `EnvTTL (Ms millis) -> "ttl", `Int millis))
        options
    in
    [ map (fun (Environment.EID eid) -> "env", `Int eid) eid
    ; map (fun s -> "code", `String s) code
    ; map (fun b -> "ret", `Bool b) return
    ]
    |> List.fold_left (fun msg -> map_or msg (fun opt -> opt :: msg)) envOpts
    |> fun ll -> Message (`Assoc (("mid", `Int mid) :: ll))
end

module Response = struct
  type prints =
    [ `Stderr of string
    | `Stdout of string
    ]
  [@@deriving eq, show { with_path = false }]

  type values =
    [ `Return of json
    | `Value of string option * json
    ]
  [@@deriving eq, show { with_path = false }]

  type error = [ `Error of string ] [@@deriving eq, show { with_path = false }]

  type t =
    [ prints
    | values
    | error
    ]
  [@@deriving eq, show { with_path = false }]

  type routable =
    { env : Environment.id
    ; mid : Message.id option
    ; data : t
    }
  [@@deriving show { with_path = false }]

  let routable_of_json mstr =
    (* Format.printf "received: %s@." mstr; *)
    let read_message mstr =
      let open Yojson.Basic in
      try Some (from_string mstr) with Yojson.Json_error _msg -> None
    in
    let destr_message = function
      | `List (routing :: rest) ->
        let env, mid =
          match routing with
          | `Int eid                     -> Environment.EID eid, None
          | `List [ `Int eid; `Int mid ] -> Environment.EID eid, Some (Message.MID mid)
          | _                            -> failwith "bad routing"
          (* TODO better error handling pls *)
        in
        { env
        ; mid
        ; data =
            (match rest with
            | [ `String "return"; value ] -> `Return value
            | [ `String "stdout"; `String output ] -> `Stdout output
            | [ `String "stderr"; `String output ] -> `Stderr output
            | [ `String "error"; `String err ] -> `Error err
            | [ `String "value"; value ] -> `Value (None, value)
            | [ `String "value"; value; `String annot ] -> `Value (Some annot, value)
            (* TODO better error handling pls *)
            | _ -> failwith "bad message")
        }
      | _                       -> failwith "bad message"
    in
    Option.bind (read_message mstr) (fun msg ->
        try Some (destr_message msg) with Failure _msg -> None)
end

module Process = struct
  type pushFun = Response.t option -> unit

  type option =
    [ envOption
    | `ModulePath of string
    ]

  (* TODO someday maybe switch over to using domain sockets; I don't think there's ever a rationale
     for using a TCP server after all (viz. increased client complexity & inherent TCP overhead
     making that route less efficient than stdio or domain sockets) *)
  type t =
    | Stdio of
        { process : Lwt_process.process
        ; consumers : pushFun Message.Map.t Environment.Map.t ref
        }

  let scriptPath () =
    let source = Option.get @@ Resources.read "server.js" in
    let f = Filename.temp_file "homunculus" ".js" in
    let oc = open_out f in
    output_string oc source;
    flush oc;
    f

  let routeResponse consumers Response.({ env; mid; data } as _resp) =
    (* need "dead letter queue" for messages that have no corresponding environment and/or mid in
       the known consumers, *and* for lines of (stdout) output that can't be parsed as JSON *)
    (* Format.printf "%a@." Response.pp_routable resp; *)
    match mid with
    | None     -> failwith "Cannot route message without mid"
    | Some mid ->
      let open Option in
      Environment.Map.find_opt env !consumers
      |> iter
           (Message.Map.find_opt mid
           %> iter (fun pushFun ->
                  pushFun (Some data);
                  match data with
                  | `Return _ | `Error _ ->
                    pushFun None;
                    consumers :=
                      Environment.Map.update env
                        (Option.map (Message.Map.remove mid))
                        !consumers
                  | _                    -> ()))
  (* Format.printf "%a@." pp_routableResponse resp *)

  (* TODO let people capture stderr *)
  let start ?options () =
    let options = Option.get_lazy (const []) options in
    let paths, args =
      List.fold_left
        (fun (paths, args) -> function
          | `ModulePath _ as path -> path :: paths, args
          | `NoCapture _          -> paths, args
          | `EnvTTL (Ms millis)   -> paths, Format.asprintf "--ttl=%d" millis :: args)
        ([], []) options
    in
    let env =
      match List.rev paths with
      | []    -> None
      | paths ->
        Some
          [| "NODE_PATH="
             ^ String.concat ":"
             @@ List.map (function `ModulePath p -> p) paths
          |]
    in
    let args = scriptPath () :: args in
    let process =
      Lwt_process.(open_process ~stderr:`Keep ?env ("", Array.of_list ("node" :: args)))
    in
    let consumers = ref Environment.Map.empty in
    let proc = Stdio { process; consumers } in
    Lwt.async (fun () ->
        Lwt_io.read_lines process#stdout
        |> Lwt_stream.iter
             (Response.routable_of_json
             %> function None -> () | Some msg -> routeResponse consumers msg));
    proc
  (* Server
     { process; port = Scanf.sscanf (input_line stdout) "%[^:]:%d" (fun _ i -> i) } *)

  let stop (Stdio { process; consumers }) =
    process#terminate;
    Environment.Map.iter
      (fun _eid -> Message.Map.iter (fun _mid pushFun -> pushFun None))
      !consumers;
    consumers := Environment.Map.empty

  let send ?(close = false) eid (Stdio { process; consumers } as p) mid (msg : jsonObject)
      =
    let msg = Message.to_string msg in
    (* Format.printf "sending %b %s@." close msg; *)
    let open Lwt.Infix in
    let responses, push = Lwt_stream.create () in
    consumers :=
      Environment.Map.update eid
        Message.Map.(
          function None -> Some (singleton mid push) | Some mm -> Some (add mid push mm))
        !consumers;
    Lwt.async (fun () ->
        Lwt_io.write_line process#stdin msg
        >>= fun _ ->
        Lwt_io.flush process#stdin
        >>= fun _ ->
        if close
        then
          Lwt_io.close process#stdin
          >>= fun _ ->
          Lwt_stream.iter (function `Return _ | `Error _ -> stop p | _ -> ()) responses
        else Lwt.return ());
    responses
end

let messageCounter = counter 0

let eval ?process ?(eid = Environment.EID 0) ?options ?return code =
  let mid = Message.MID (messageCounter ()) in
  let msg = Message.(to_json @@ create ?eid:(Some eid) ~code ?return ?options mid) in
  match process with
  | Some p -> Process.send eid p mid msg
  | None   ->
    let p = Process.start ?options () in
    Process.send ~close:true eid p mid msg

type aggregate =
  { all : Response.t list
  ; prints : Response.prints list
  ; values : (string option * json) list
  ; result : (json, string) result
  }

let aggregated_result responses =
  let open Lwt in
  let new_result before after =
    match before with
    | None   -> Some after
    | Some _ ->
      failwith
        "Encountered more than one error or return in homunculus eval responses, should \
         be impossible"
  in
  Lwt_stream.fold_s
    (fun resp (all, prints, values, result) ->
      let all = resp :: all in
      match resp with
      | #Response.prints as m -> return (all, m :: prints, values, result)
      | `Value pair           -> return (all, prints, pair :: values, result)
      | `Error m              -> return (all, prints, values, new_result result @@ Error m)
      | `Return v             -> return (all, prints, values, new_result result @@ Ok v))
    responses ([], [], [], None)
  >|= fun (all, prints, values, result) ->
  match result with
  | None        ->
    failwith "No result or error in homunculus eval responses, should be impossible"
  | Some result ->
    List.{ all = rev all; prints = rev prints; values = rev values; result }

let return_result responses =
  Lwt.map (fun { result; _ } -> result) @@ aggregated_result responses
