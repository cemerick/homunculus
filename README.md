
## Modes of operation

### One-shot

1. Exec node process
2. Evaluate submitted snippet, capturing stdout/stderr
3. Stream emitted values on real stdout until snippet's promise(s) are exhausted
4. Emit stdout/stderr content
5. Node process termination/cleanup

### Persistent

1. Exec node process (only once)
2. Stand up evaluation server
3. Accept eval connection/message
4. Process steps 2-4 of the above one-shot flow, with:
   a. evaluation occurring within a discrete, isolated environment corresponding to a context ID
      indicated in the evaluation message
   b. emitted values being returned via the eval connection instead of stdout

Persistent operation also requires a `free` message type, so that the OCaml program can release the
associated environment. By default, environments should probably be capped at some configurable
number and/or be subject to periodic sweeps.

## References

* https://github.com/tweag/inline-js
* http://foil.sourceforge.net/
* https://aws.github.io/jsii/
