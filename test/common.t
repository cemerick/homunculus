
Most basic operation:

  $ ./harness.sh <<END
  > "return 5"
  > END
  [0,"return",5]

Captured stderr/stdout:

  $ ./harness.sh <<END
  > "console.log('foo')"
  > "console.error('bar')"
  > END
  [0,"stdout","foo\n"]
  [0,"return",null]
  [0,"stderr","bar\n"]
  [0,"return",null]

Explicit environment reference/creation:

  $ ./harness.sh <<END
  > {"env":1, "code":"return 5"}
  > END
  [1,"return",5]

Separate environments:

  $ ./harness.sh <<END
  > {"env":1, "code":"k = 5"}
  > {"env":2, "code":"k = 6"}
  > {"env":1, "code":"return k"}
  > {"env":2, "code":"return k"}
  > END
  [1,"return",null]
  [2,"return",null]
  [1,"return",5]
  [2,"return",6]

Fire-and-forget (no return value):

  $ ./harness.sh <<END
  > {"code":"k = 5", "ret":false}
  > {"code":"return k"}
  > END
  [0,"return",5]

Suppressing stdout/stderr capture:

  $ ./harness.sh <<END
  > {"code":"console.error('foo')", "ret":false}
  > {"stdout":false, "code":"console.log('bar')", "ret":false}
  > END
  [0,"stderr","foo\n"]

Empty expression:

  $ ./harness.sh <<END
  > {"code":""}
  > END
  [0,"return",null]

Can send configuration-only messages:

  $ ./harness.sh <<END
  > {"code":"console.log('foo')", "ret":false}
  > {"stderr":false}
  > {"code":"console.error('bar')", "ret":false}
  > END
  [0,"stdout","foo\n"]

Non-return emitted values:

  $ ./harness.sh <<END
  > "homunculus.emit(5);homunculus.emit(6)" 
  > END
  [0,"value",5]
  [0,"value",6]
  [0,"return",null]

Non-return emitted values with annotations:

  $ ./harness.sh <<END
  > {"ret":false, "code":"homunculus.emit(5, 'first');homunculus.emit(6, 'second')" }
  > END
  [0,"value",5,"first"]
  [0,"value",6,"second"]

Explicitly closing environments:

  $ ./harness.sh <<END
  > {"env":1, "code":"k = 5", "ret":false}
  > {"env":1, "code":"return k"}
  > {"kill":1}
  > {"env":1, "code":"return k"}
  > END
  [1,"return",5]
  [1,"error","ReferenceError: k is not defined"]

Message IDs:

  $ ./harness.sh <<END
  > {"mid": 8, "code":"setTimeout(() => { console.log('hi'); homunculus.emit(5, 'annot') }); return 42" }
  > END
  [[0,8],"return",42]
  [[0,8],"stdout","hi\n"]
  [[0,8],"value",5,"annot"]

Properly report code compilation errors:

  $ ./harness.sh <<END
  > {"code":"<=>" }
  > END
  [0,"error","SyntaxError: Unexpected token '<='"]
