open Containers
open OUnit2
open Fun
module Homunculus = Homunculus__Impl

let asprintf = Format.asprintf

(* type case =
     { input : Yojson.Basic.t list
     ; output : routable list
     }
   [@@deriving show { with_path = false }]

   let cases =
     let open String in
     let open Str in
     let lines = trim %> lines in
     IO.(with_in "common.t" read_all)
     |> split (regexp "\n\n.+\n\n")
     |> List.map (global_replace (regexp "^ +\\(> \\)?") "")
     |> List.map (fun s ->
            let start = find ~sub:"END" s in
            let stop = find ~sub:"END" ~start:(start + 3) s in
            { input =
                sub s (start + 3) (stop - start - 3)
                |> lines
                |> List.map Yojson.Basic.from_string
            ; output =
                sub s (stop + 3) (length s - stop - 3)
                |> lines
                |> List.map Homunculus.Response.routable_of_json
            }) *)

let pp_list f =
  let open Format in
  let ps s fmt () = pp_print_string fmt s in
  List.pp ~pp_start:(ps "[") ~pp_stop:(ps "]") ~pp_sep:(ps "; ") f

let assert_responses =
  assert_equal
    ~cmp:(List.equal Homunculus.Response.equal)
    ~printer:(asprintf "%a" @@ pp_list Homunculus.Response.pp)

(* evaluations that are self-contained, should have same results whether doing one-shot evals
   against transient node processes or with a long-lived process, even in the same environment *)
let isolated_evals =
  [ ( "hello world"
    , {|console.log('hi');
        homunculus.emit(5, 'annot');
        return 42|}
    , [ `Stdout "hi\n"; `Value (Some "annot", `Int 5); `Return (`Int 42) ] )
  ]

let env_evals =
  isolated_evals
  @ [ "set value", "k = 5", [ `Return `Null ]
    ; "get value", "return k", [ `Return (`Int 5) ]
    ; "error", "not_defined", [ `Error "ReferenceError: not_defined is not defined" ]
    ]

let test_stream ?process ~sequential cases _ctxt =
  let open Lwt.Infix in
  let test_case (name, code, expected) =
    Lwt_unix.sleep (Stdlib.Random.float 0.25)
    >>= fun _ ->
    Homunculus.eval ?process code
    |> Lwt_stream.to_list
    (* >|= (fun r -> Format.printf "%a@." (pp_list Homunculus.Response.pp) r; r) *)
    >|= assert_responses ~msg:(asprintf "failed `%s`" name) expected
  in
  Lwt_list.(if sequential then iter_s else iter_p) test_case cases

let withProcess f ctxt =
  let process =
    bracket
      (fun _ -> Homunculus.Process.start ())
      (fun p _ -> Homunculus.Process.stop p)
      ctxt
  in
  f process ctxt

let _ =
  OUnit2.(
    run_test_tt_main
      ("homunculus"
      >::: [ "transient"
             >:: OUnitLwt.lwt_wrapper @@ test_stream ~sequential:false isolated_evals
           ; "long-running process, single environment"
             >:: OUnitLwt.lwt_wrapper
                 @@ withProcess (fun process ctxt ->
                        test_stream ~process ~sequential:true env_evals ctxt)
           ; "concurrent spamming"
             >:: OUnitLwt.lwt_wrapper
                 @@ withProcess (fun process ctxt ->
                        let numbers = List.(0 -- 50) in
                        let spam_evals =
                          List.map
                            (fun i ->
                              ( asprintf "inc %d" i
                              , asprintf "x.push(%d);return null" i
                              , [ `Return `Null ] ))
                            numbers
                        in
                        let open Lwt.Infix in
                        let open Homunculus in
                        eval ~process "x = []"
                        |> return_result
                        >>= fun _ ->
                        test_stream ~process ~sequential:false spam_evals ctxt
                        >>= fun _ ->
                        eval ~process "return x"
                        |> return_result
                        >|= function
                        | Ok vals ->
                          let a = Result.get_exn @@ [%of_yojson: int list] vals in
                          assert_bool "evaluations were sent in order"
                            (not @@ List.is_sorted ~cmp:Int.compare a);
                          let sorted = List.sort Int.compare a in
                          assert_equal
                            ~msg:"some evaluation(s) were not sent or not effectful"
                            ~cmp:(List.equal Int.equal) numbers sorted
                        | _       -> assert_failure "unexpected result")
             (* this is by no means a realistic "benchmark"; just making sure the various bits
                involved work together under a mix of a measure of compute, IO, logging, etc.
                To wit, this test exposed a bug in node versions 13+ (https://github.com/nodejs/node/issues/40999)!!
                (_very_ ymmv, but this test runs @ 12K msgs/s on my very old laptop, so :thumbsup: IMO ) *)
             (* TODO this issue has been resolved as of 07/2022 *)
           ; "stress"
             >:: OUnitLwt.lwt_wrapper
                 @@ withProcess (fun process ctxt ->
                        let open Lwt.Infix in
                        let open Homunculus in
                        let changedFileLength =
                          IO.(with_in "test.ml" read_all)
                          |> String.replace ~sub:" " ~by:""
                          |> String.length
                        in
                        let scale = 5000 in
                        eval ~process "fs = require('fs')"
                        |> return_result
                        >>= fun _ ->
                        Iter.(
                          init (fun _ ->
                              ( "stress"
                              , {|console.log("ok");
                                  return fs.readFileSync("test.ml", "UTF8").replace(/ /g, "").length|}
                              , [ `Stdout "ok\n"; `Return (`Int changedFileLength) ] ))
                          |> take scale
                          |> to_list
                          |> Lwt.return)
                        >>= fun evals ->
                        let t = Sys.time () in
                        test_stream ~process ~sequential:false evals ctxt
                        >|= fun _ ->
                        Format.printf "%f msgs/s@."
                          (float_of_int scale /. (Sys.time () -. t)))
           ]))
