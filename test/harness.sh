#!/bin/sh


set -e

INPUT="`cat /dev/stdin`"
STDIO="`echo \"$INPUT\" | node ../src/server.js`"

node ../src/server.js -q --on sock & 
PID=$!
inotifywait -qq -e create .
# `nc -N` kills the connection too soon, leading to "write after end" errors; in a server mode,
# the node process doesn't exit once the eval's promises have been exhausted, so we need to do
# an absurd wait-and-kill dance
OUT=`mktemp`
(echo "$INPUT" | nc -U sock > $OUT &)
sleep 0.1
kill $PID
# server still sometimes doesn't clean up after being killed, despite the `.on('exit',...)` handler
rm sock 
SERVER=`cat $OUT`

if [ "$STDIO" = "$SERVER" ]; then
  /bin/echo "$SERVER"
else
  echo "** Mismatch between stdio and server **"
  echo stdio:
  /bin/echo "$STDIO"
  echo
  echo server:
  /bin/echo "$SERVER"
fi