Supplying a single expression for one-shot operation:

  $ node ../src/server.js -e "return 5"
  [1,"return",5]


